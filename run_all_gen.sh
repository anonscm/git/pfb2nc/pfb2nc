#!/bin/bash

#name0="Vshape_exp_20m"
#name1="rightbank_H2_clay_BF_sandy_higher_n"
#name2="rightbank_H2_clay_BF_sandy_higher_n"
#name3="2016_06_29"

#name0="gravi_cube_two_hillslopes"
#name1="two_hillslopes_11yrs_steep_H9"
#name2="gravi_11yrs_steep_H9"
#name3="2016_06_02"

#name0="gravi_cube_two_hillslopes"
#name1="tmp"
#name2="tmp"
#name3="2016_06_02"

#name0="Oueme_sup"
#name1="oueme_5yrs"
#name2="oueme_5yrs"
#name3="2016_05_17"

#name0="Oueme_sup"
#name1="tmp"
#name2="tmp"
#name3="2016_06_03"

#name0="hillslope"
#name1="results"
#name2="hillslope"
#name3="2016_06_03"

name0="Donga"
name1="results2017"
name2="Donga"
name3="2017_11_30"

#name0="1D_vegtest"
#name1="1Dlike_vshape_rightbank_520_H3_n22_K01_lowP"
#name2="1Dlike_vshape_rightbank_520_H3_n22_K01_lowP"
#name1="1Dlike_vshape_rightbank_520_H3_n22_lowP"
#name2="1Dlike_vshape_rightbank_520_H3_n22_lowP"
#name1="1Dlike_vshape_rightbank_520_H3_n22_K01"
#name2="1Dlike_vshape_rightbank_520_H3_n22_K01"
#name3="2016_10_18"

#PF_ntimesteps="1320"
#PF_ntimesteps="3897"
#11yrs +1 day
#PF_ntimesteps="4016"
#8yrs +1 day
PF_ntimesteps="2921"
#5yrs +1 day
#PF_ntimesteps="1826"
#3yrs +1 day
#PF_ntimesteps="1096"
#2yrs +1 day
#PF_ntimesteps="731"
#1yrs +1 day
PF_ntimesteps="366"
#10 d +1
#PF_ntimesteps="11"

#PF_timesteps="1"
PF_timesteps="24"

#outdir="/home/hector/scripts_matlab/outputs/"
outdir="/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/NCoutputs/"
#outdir="/scratch/basileh/outputs/"
#CLM_ntimesteps="52560"
#2yrs
#CLM_ntimesteps="17520"
#CLM_ntimesteps="17280"
#5yrs
#CLM_ntimesteps="43800"
#8yrs
CLM_ntimesteps="70080"
#11yrs
#CLM_ntimesteps="96360"
#1yrs
CLM_ntimesteps="8760"
#10d
#CLM_ntimesteps="240"

#CLM_ntimesteps="40678"
#CLM_ntimesteps="13921"
#CLM_ntimesteps="6864"
CLM_timesteps="1"

CLMnvar="11"
CLMvar="0 1 2 3 4 5 6 7 8 11 12"
#CLMnvar="8"
#CLMvar="0 2 3 4 5 6 7 8"
#CLMnvar="3"
#CLMvar="7 8 12"
#CLMnvar="2"
#CLMvar="8 12"
#CLMnvar="1"
#CLMvar="4"
#CLMvar="12"

#CLMvar="0"
# SATURATION FILE:
rm input_satur_gen.txt
#echo "/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/"$name0"/"$name1"/">> input_satur_gen.txt
echo "/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/"$name1"/">> input_satur_gen.txt
echo "doro" >> input_satur_gen.txt
echo "satur" >> input_satur_gen.txt
echo "saturation" >> input_satur_gen.txt
echo "-" >> input_satur_gen.txt
#echo "-9999" >> input_satur_gen.txt
echo "0" >> input_satur_gen.txt
echo $PF_ntimesteps >> input_satur_gen.txt
echo $PF_timesteps >> input_satur_gen.txt
echo $outdir >> input_satur_gen.txt
echo $name2 >> input_satur_gen.txt
echo "" >> input_satur_gen.txt
echo "6" >> input_satur_gen.txt
echo "dgridZ -" >> input_satur_gen.txt
echo "porosity -" >> input_satur_gen.txt
echo "specific_storage -" >> input_satur_gen.txt
echo "permeability m/hr" >> input_satur_gen.txt
echo "var_dz -" >> input_satur_gen.txt
echo "mask -" >> input_satur_gen.txt
echo "" >> input_satur_gen.txt
echo "4" >> input_satur_gen.txt
echo "veg_map -" >> input_satur_gen.txt
echo "xslope m/m" >> input_satur_gen.txt
echo "yslope m/m" >> input_satur_gen.txt
echo "top_mask -" >> input_satur_gen.txt

# PRESSURE FILE:
rm input_press_gen.txt
#echo "/home/hector/simus/"$name0"/"$name1"/">> input_press_gen.txt
echo "/home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/"$name1"/">> input_press_gen.txt
echo "doro" >> input_press_gen.txt
echo "press" >> input_press_gen.txt
echo "succion" >> input_press_gen.txt
echo "m" >> input_press_gen.txt
echo "-999999" >> input_press_gen.txt
echo $PF_ntimesteps >> input_press_gen.txt
echo $PF_timesteps >> input_press_gen.txt
echo $outdir >> input_press_gen.txt
echo $name2 >> input_press_gen.txt
echo "" >> input_press_gen.txt
echo "6" >> input_press_gen.txt
echo "dgridZ -" >> input_press_gen.txt
echo "porosity -" >> input_press_gen.txt
echo "specific_storage -" >> input_press_gen.txt
echo "permeability m/hr" >> input_press_gen.txt
echo "var_dz -" >> input_press_gen.txt
echo "mask -" >> input_press_gen.txt
echo "" >> input_press_gen.txt
echo "4" >> input_press_gen.txt
echo "veg_map -" >> input_press_gen.txt
echo "xslope m/m" >> input_press_gen.txt
echo "yslope m/m" >> input_press_gen.txt
echo "top_mask -" >> input_press_gen.txt

# CLM FILE:
rm input_CLM_gen.txt
#echo "/home/hector/simus/"$name0"/"$name1"/" >> input_CLM_gen.txt
echo "/home/hectorb/PARFlOW/PROJECTS/Donga/Donga_1km/output/"$name1"/" >> input_CLM_gen.txt
echo "doro" >> input_CLM_gen.txt
echo $CLMnvar >> input_CLM_gen.txt
echo $CLMvar >> input_CLM_gen.txt
echo "" >> input_CLM_gen.txt
echo $CLM_ntimesteps >> input_CLM_gen.txt
echo $CLM_timesteps >> input_CLM_gen.txt
echo $outdir >> input_CLM_gen.txt
echo $name2 >> input_CLM_gen.txt
echo "" >> input_CLM_gen.txt
echo "2" >> input_CLM_gen.txt
echo "top_mask -" >> input_CLM_gen.txt
echo "veg_map -	" >> input_CLM_gen.txt


./PFBtoNC input_satur_gen.txt
./PFBtoNC input_press_gen.txt
./ClmPFBtoNC input_CLM_gen.txt

#1D
#cp /home/hector/simus/$name0/$name1/forcagePF.txt.0 /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /home/hector/simus/$name0/$name1/vegtest.tcl /home/hector/scripts_matlab/outputs/$name2"_"$name3/simu.tcl
#cp /home/hector/simus/$name0/$name1/*domains.pfb /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2"_"domains.pfb

#Vshape
#cp /home/hector/simus/$name0/$name1/forcagePF.txt.0 /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /home/hector/simus/$name0/$name1/vshape.tcl /home/hector/scripts_matlab/outputs/$name2"_"$name3/simu.tcl
#cp /home/hector/simus/$name0/$name1/*domains.pfb /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2"_"domains.pfb

# Cube gravi

#cp /home/hector/simus/$name0/$name1/forcagePF.txt.0 /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /home/hector/simus/$name0/$name1/cube.tcl /home/hector/scripts_matlab/outputs/$name2"_"$name3/simu.tcl
#cp /home/hector/simus/$name0/$name1/cube.pfb /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2"_"domains.pfb

# Oueme
#cp /home/hector/simus/$name0/$name1/forcagePF.txt.0 /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /home/hector/simus/$name0/$name1/oueme.tcl /home/hector/scripts_matlab/outputs/$name2"_"$name3/simu.tcl
#cp /home/hector/simus/$name0/$name1/oueme_domains.pfb /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2"_"domains.pfb

#hillslope
#cp /scratch/basileh/simus/$name0/$name1/forcagePF.txt.0 /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /scratch/basileh/simus/$name0/$name1/hillslope.tcl /home/hector/scripts_matlab/outputs/$name2"_"$name3/simu.tcl
#cp /scratch/basileh/simus/$name0/$name1/oueme_domains.pfb /home/hector/scripts_matlab/outputs/$name2"_"$name3/$name2"_"domains.pfb

#Donga
cp /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/$name1/forcagePF.txt.0 /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/NCoutputs/$name2"_"$name3/$name2""forcagePF.txt
cp /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/$name1/donga.tcl /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/NCoutputs/$name2"_"$name3/simu.tcl
cp /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/$name1/donga_domains.pfb /home/hectorb/PARFLOW/PROJECTS/Donga/Donga_1km/output/NCoutputs/$name2"_"$name3/$name2"_"domains.pfb
