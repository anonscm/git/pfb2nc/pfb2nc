/*********************************************************************************************

CONVERSION BIG ENDIANS to LITTLE ENDIANS

Les fichiers pfb generes par PARFLOW sont des fichiers binaires en BIG ENDIANS

V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Mars 2015

**********************************************************************************************/


 

double swapDouble(double d)
{
   double a;
   unsigned char *dst = (unsigned char *)&a;
   unsigned char *src = (unsigned char *)&d;

   dst[0] = src[7];
   dst[1] = src[6];
   dst[2] = src[5];
   dst[3] = src[4];
   dst[4] = src[3];
   dst[5] = src[2];
   dst[6] = src[1];
   dst[7] = src[0];

   return a;
}

int swapInt(int i)
{
   int a;
   unsigned char *dst = (unsigned char *)&a;
   unsigned char *src = (unsigned char *)&i;

   dst[0] = src[3];
   dst[1] = src[2];
   dst[2] = src[1];
   dst[3] = src[0];
   
   return a;
}
