/*********************************************************************************************

creatNCsoil (path generic_name data_name file_number DumpInterval ficher.nc *name_var2D *att_var2D)

path : 			chemin d'accès des donnée
generic_name	:	nom generique de la simulation utilisé pour nommer les fichiers PF
numberData	:	nombre de données à traiter
numbers_dataName:	liste des numeros de donnée
file_number	:	nombre de fichiers à lire
DumpInterval	:	Pas d'échantillonnage, utilisé pour lire les fichiers. 
			Ex une sortie en saturation à 24h: 
			genname.out.satur.00000.pfb genname.out.satur.00024.pfb...
fichier		:	nom du fichier netcdf à ecrire 
			(resultat = 1 fichier par variable ex: fichier_eflx_lh_tot.nc)

name_var2D	:	liste de variables 2D à traiter
name_var2D	:	liste de unités de variables 2D à traiter

...

V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Avril 2016


**********************************************************************************************/

using namespace std;

//void creatNCsoil ( string pth, string generic_name, string dataname, int numbers_dataName ,int file_number, int DumpInterval, string ficher.nc, string *name_var2D, string *att_var2D);
int creatNCsoil ( string , string , string ,int ,  int , int , string, string , string*, string*);
