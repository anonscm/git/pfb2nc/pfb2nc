/*********************************************************************************************
PFBtoNCV4


Le programme PFBtoNCV4 permet la conversion d'une série
de fichier .pfb, générés par PARFLOW, en un fichier au format NETCDF.
Cette version permet d'appliquer un masque et d'integrer au fichier NETCDF des variables 2D et 3D.
Les parametres d'entres de PFBtoNCV4 sont contenus dans un fichier input.txt.

SYNTAX : PBFtoNCV4 input.txt

CONTENU du fichier input.txt :

path 			# chemin d'accès des donnée
generic_name		# nom generique de la simulation utilisé pour nommer les fichiers PF
data_name		# nom de la donnée
ncdata_name		# nom de la variable netcdf à écrire
units			# unités de la donnee
missing values		# missing values
file_number		# nombre de fichiers à lire
DumpInterval		# pas d'échantillonnage
root_dir_out		# chemin d'accès des données à écrire
gen_out 		# prefixe générique du fichier netcdf (.nc) à ecrire
                        #
numberVar3D             # nombre de varaibles 3D a traiter puis pour chaque variable
name unit               # nom de la variable unité de la variable
                        #
numberVar2D             # nombre de varaibles 2D a traiter puis pour chaque variable
name unit               # nom de la variable unité de la variable
                        #

#Exemple pas d'échantillonnage d'une sortie en saturatio a 24h : genname.out.satur.00000.pfb genname.out.satur.00024.pfb...

...

V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Mars 2015

Modif B. HECTOR Février 2016
Modif Version2 V.QUATELA Mars 2016
Modif Version3 V.QUATELA Mars 2016
Modif Version4 V.QUATELA Mars 2016
**********************************************************************************************/


#include <iostream>
#include<cstdio>
#include <cstdlib>
#include"conversionBigEndiansToLittleEndians.h"
#include <netcdfcpp.h>
#include <sstream>
#include <iomanip>

#include <ctime>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

static const int NC_ERR = 2; // Retour code erreur



    int main (int argc, char *argv[]) 

    {    
	if (argc != 2)
	cout << "SYNTAX : PFBtoNCV4 input.txt " << endl;
	else
	{
	
		string input_file = argv[1];  
		ifstream fichier(input_file.c_str(), ios::in);
	
		if(!fichier)  
		{
		cout << "Impossible d'ouvrir le fichier "<< input_file  << endl;
		return 0;
		}
	
		else
		{




	

//"""""""""""""""""""""""  LECTURE INPUT.TXT """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		cout<<endl;
		cout<<"LECTURE DU FICHIER DE PARAMETRES " << endl;
		
		string pth, gen, gen_out, root_dir_out, dataname, ncdataname, units, missing_values;
		stringstream out_dir, fichierNC;
		int NTIME, dumpint, numberVar3D, numberVar2D;
		char curr_date [50];
		
        	fichier >> pth >> gen >> dataname >> ncdataname >> units >> missing_values >> NTIME >> dumpint >> root_dir_out >> gen_out >> numberVar3D;
       		cout <<" Liste de parametres = "<< pth<<" "<< gen <<" "<< dataname <<" "<<ncdataname <<" "<< units<< " "<< missing_values<<" "<< NTIME <<" "<< dumpint <<" "<< root_dir_out <<" "<< gen_out <<endl<<endl;  
    		time_t t = time(0);   // get time now
		struct tm * now = localtime( & t );
		
		sprintf(curr_date,"%4d_%02d_%02d",(now->tm_year + 1900),(now->tm_mon + 1),now->tm_mday);
                out_dir <<root_dir_out<<gen_out<<"_" << curr_date << "/";  // création du nouveau dossier


		mkdir(out_dir.str().c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);


		fichierNC << out_dir.str().c_str() << gen_out <<"_" << dataname << ".nc";

 		string name_var_3D[numberVar3D];
 		string att_var_3D[numberVar3D];
 		
 		cout<<"nombre variables 3D = "<< numberVar3D << endl;
 		
 		for (int s = 0; s < numberVar3D ; s++)
 		{
 			fichier >> name_var_3D[s] >> att_var_3D[s];
 			cout<< name_var_3D[s] <<" "<< att_var_3D[s]<<endl;
 		} 
	
		fichier >> numberVar2D;
		cout<<endl;
		cout<<"nombre variables 2D = "<< numberVar2D << endl;
		
		string name_var_2D[numberVar2D];
 		string att_var_2D[numberVar2D];
		
		for (int t = 0; t < numberVar2D ; t++)
 		{
 			fichier >> name_var_2D[t] >> att_var_2D[t];
 			cout<< name_var_2D[t] <<" "<< att_var_2D[t]<<endl;
 		}
                

            	fichier.close();	
		cout<<endl;
	
	
//""""""""""""""""""""""""""" LECTURE PREMIER PBF """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
     		
     			
  		string fin_fichier = ".pfb"; 	// extension du fichier
		stringstream filename;
		filename << pth << gen << ".out." << dataname << ".00000" << fin_fichier;  // création du nouveau nom

     		cout<<"LECTURE DES DIMENSIONS dans fichier pfb = " << filename.str().c_str()<<endl<<endl;;
		FILE * f;
		double X0[3]; 		// X,Y,Z origine
		int NX, NY, NZ;		// nombre de maille NX, NY, NZ;
		double D[3];	 	// dimension de maille DX, DY,DZ
		int I[3];		// ix, iy, iz;
		int n[3];		// nx, ny, nz;
		int r[3];		// rx, ry, rz;	
		int num_subgrids;	// nombre de sous grille
		
		
		f = fopen(filename.str().c_str(), "rb");
      		if (f == NULL)
        	cout << "Impossible d'ouvrir le fichier en lecture !" << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
			
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}
			
			num_subgrids = swapInt(num_subgrids);
					
			cout << " Point Origine : X=" << X0[0] <<" "<< "Y=" << X0[1]<< " " << "Z=" << X0[2]<< endl;
        		cout << " Nombre de X,Y,Z : NX=" << NX <<" "<< "NY=" << NY << " " << "NZ=" << NZ << endl; 
        		cout << " Dimensions : DX=" << D[0] <<" "<< "DY=" << D[1] << " " << "DZ=" << D[2] << endl;
			cout << " Nombre de mailles = "<< NX*NY*NZ << endl;
			cout << " Nombre de sous grille = "<< num_subgrids<<endl<< endl;
			fclose (f);
		}

		cout<<"PREMIERE LECTURE OK " <<endl<<endl;
		
		double data[NX*NY*NZ]; // stockage donnees lues
				
		//""""""""""""""""""""""""""""""""""""" PREPARATION DES DONNEES NETCDF """"""""""""""""""""""""""""""""""""""""""
		
		double dataNC[NZ][NY][NX];
				
   		double dataTemp[NZ][NY][NX];
		double dataTemp2[NY][NX];

			
		// Creation lats, lons, depths
		
		
		//int NTIME =atoi(argv[4]);
		
   		double lons[NX],lats[NY],depths[NZ],times[NTIME];
		
   				
		for (int i = 0; i < NX ; i++)
		{
			lons[i]= X0[0] + (i*D[0]) ;
		}
   		  
		for (int i = 0; i < NY ; i++)
		{
			lats[i]= X0[1] + (i*D[1]) ;
		}
   		
		for (int i = 0; i < NZ ; i++)
		{
			depths[i]= X0[2] + (i*D[2]) ;
		}
		for (int i = 0; i < NTIME ; i++)
		{
			times[i]= i;
		}

			
				
//""""""""""""""""""""""""""""""""""""" CREATION FICHIER NETCDF """"""""""""""""""""""""""""""""""""""""""""""""""  			

		cout << "CREATION FICHIER NETCDF = " << fichierNC.str().c_str() << endl<< endl;
	
		// Creation du fichier
   		NcFile dataFile(fichierNC.str().c_str(), NcFile::Replace);

   		// Vérification de la création du fichier
   		if(!dataFile.is_valid())
      		return NC_ERR;
      		
      		// Definition des attributs globaux
      		if(!dataFile.add_att("Conventions", "CF-1.5"))
      		return NC_ERR;
		if(!dataFile.add_att("title", "Soil saturation and pressure head calculated by Parflow/CLM 3D hydrological model"))
      		return NC_ERR;
      		if(!dataFile.add_att("institution", "LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE"))
      		return NC_ERR;
      		if(!dataFile.add_att("source", "Generated by J-M. Cohard: Parflow/CLM Simulation"))
      		return NC_ERR;
      		if(!dataFile.add_att("references", "Robert, D. (2012). Caractérisation et modélisation de la dynamique de l evapotranspiration"
      		" en Afrique Soudanienne en zone de socle. Terre Univers Environnement. Grenoble, France, Université de Grenoble. PhD Thesis: 199 pp."))
      		return NC_ERR;
	
   		// Definition des dimensions
   		NcDim *timeDim, *lonDim, *latDim, *depthDim ;
      		
      		if (!(timeDim = dataFile.add_dim("time")))  // dimension = unlimited 
      		return NC_ERR;
       		if (!(lonDim = dataFile.add_dim("longitude", NX )))
      		return NC_ERR;
       		if (!(latDim = dataFile.add_dim("latitude", NY)))
      		return NC_ERR;
      		if (!(depthDim = dataFile.add_dim("depth", NZ )))
      		return NC_ERR;
   
   		// Definition des variables.

		

   		NcVar *lonVar, *latVar, *depthVar, *timeVar, *dXVar, *dYVar, *dataVar;
   
   		if (!(timeVar = dataFile.add_var("time", ncDouble, timeDim)))
   		return NC_ERR;
   		if (!(lonVar = dataFile.add_var("longitude", ncDouble, lonDim)))
      		return NC_ERR;
    		if (!(latVar = dataFile.add_var("latitude", ncDouble, latDim)))
   		return NC_ERR;
    		if (!(depthVar = dataFile.add_var("depth", ncDouble, depthDim)))
        	return NC_ERR;   
    		if (!(dXVar = dataFile.add_var("dgridX", ncDouble,depthDim,latDim,lonDim)))
        	return NC_ERR;   
    		if (!(dYVar = dataFile.add_var("dgridY", ncDouble,depthDim,latDim,lonDim)))
        	return NC_ERR;
    		if (!(dataVar = dataFile.add_var(ncdataname.c_str(), ncDouble,timeDim,depthDim,latDim,lonDim)))
        	return NC_ERR;

		

   		// Definition des attributs de variables
   
   		if (!timeVar->add_att("long_name", "time"))
      		return NC_ERR;
   		if (!timeVar->add_att("standard_name", "time"))
	      	return NC_ERR;
	   	if (!timeVar->add_att("units", "seconde since 1970-01-01 00:00:00 utc"))
	      	return NC_ERR;
   
   		if (!lonVar->add_att("units", "degrees_east"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("long_name", "longitude"))
	      	return NC_ERR;
	  	 if (!lonVar->add_att("standard_name", "longitude"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("axis", "x"))
	      	return NC_ERR;
   
   		if (!latVar->add_att("units", "degrees_north"))
	      	return NC_ERR;
	   	if (!latVar->add_att("long_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("standard_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("axis", "y"))
	     	return NC_ERR;

	   	if (!depthVar->add_att("units", "meter"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("long_name", "depth from the surface"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("standard_name", "depth"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("axis", "z"))
	      	return NC_ERR;

	     	if (!dataVar->add_att("units", units.c_str()))
	      	return NC_ERR;
	     	if (!dataVar->add_att("long_name", ncdataname.c_str()))
	      	return NC_ERR;
	     	if (!dataVar->add_att("standard_name", ncdataname.c_str()))
	      	return NC_ERR;
	     	if (!dataVar->add_att("missing_value", missing_values.c_str()))
	      	return NC_ERR;

		
		

	 	

		// Ecriture des variables coordonées time lon lat depth
	   
	      	if (!timeVar->put(times, NTIME))
	     	return NC_ERR;
	   	if (!lonVar->put(lons, NX))
	     	return NC_ERR; 
	  	if (!latVar->put(lats, NY))
	      	return NC_ERR;
	      	if (!depthVar->put(depths, NZ))
	     	return NC_ERR;
		
		// Ecriture des variables dgriX = 0 et dgriY = 0

		for (int depth = 0; depth < NZ; depth++)
		      		for (int lat = 0; lat < NY; lat++)
			 		for (int lon = 0; lon < NX; lon++)
			 		{
			    			dataTemp[depth][lat][lon] = 0;
						
			 		}

		
		 if (!dXVar->put(&dataTemp[0][0][0], NZ, NY, NX))
                        return NC_ERR;
                 if (!dYVar->put(&dataTemp[0][0][0], NZ, NY, NX))
                        return NC_ERR;







//""""""""""""""""""""""""""" TRAITEMENT des VARIABLES  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

// boucle traitement variables 3 dimensions NX NY NZ 


	for (int s = 0; s < numberVar3D ; s++)

		{
			cout << "name_var_3D = " << name_var_3D[s] << endl;

			// Lecture d'un fichier pfb utilisé pour enregistrer une nouvelle variable dans le fichier netcdf.


			stringstream var_filename;
			stringstream var_filename2;
			
			var_filename << pth << name_var_3D[s] << fin_fichier;  // création du nouveau nom
			f = fopen(var_filename.str().c_str(),"rb");
	 		
	 		int VARFILE = 0;
	 		
			if (f != NULL)
				{ VARFILE = 1;}
			else
				{
					
					var_filename2 << pth << gen << ".out."<< name_var_3D[s] << fin_fichier;
					f = fopen(var_filename2.str().c_str(),"rb");
					if (f != NULL) VARFILE = 1;
				}				
			 
			
			if (VARFILE == 0 )
				cout << "Impossible d'ouvrir le fichier variable = " << name_var_3D[s] << endl << endl;
			
					
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ= swapInt(NZ);
							
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}
					
				num_subgrids = swapInt(num_subgrids);
			
				for  (int l = 0; l < num_subgrids ; l++)
				{

					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
		
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}

					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}			
				}			
			
				fclose (f);
			
				for (int depth = 0; depth < NZ; depth++)
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)
				 		{
				    			dataTemp[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX];
						
				 		}
			
				      		
		
				//definition de la variable
				NcVar *name_var;
				
				if (!(name_var = dataFile.add_var(name_var_3D[s].c_str(), ncDouble,depthDim,latDim,lonDim)))
				return NC_ERR;

				// Definition des attributs de la variable
				if (!name_var->add_att("units", att_var_3D[s].c_str()))
		      		return NC_ERR;

				// Ecriture des variables donnees en argument
				if (!name_var->put(&dataTemp[0][0][0], NZ, NY, NX))
		                return NC_ERR;

				cout<<"creation "<< name_var_3D[s]<<" OK" <<endl<<endl;

			}

		}
	
		cout<<"fin structure fichier NC 3D " <<endl<< endl;


//boucle traitement variables 2 dimensions NX NY


	for (int t = 0; t < numberVar2D ; t++)

		{
			cout << "name_var_2D = " << name_var_2D[t] << endl;

			// Lecture d'un fichier pfb utilisé pour enregistrer une nouvelle variable dans le fichier netcdf.


			stringstream var_filename3;
			stringstream var_filename4;
			
			var_filename3 << pth << name_var_2D[t] << fin_fichier;  // création du nouveau nom
			f = fopen(var_filename3.str().c_str(),"rb");
	 		
	 		int VARFILE2D = 0;
	 		
			if (f != NULL)
				{ VARFILE2D = 1;}
			else
				{
					
					var_filename4 << pth << gen << ".out."<< name_var_2D[t] << fin_fichier;
					f = fopen(var_filename4.str().c_str(),"rb");
					if (f != NULL) VARFILE2D = 1;
				}				
			 
			
			if (VARFILE2D == 0 )
				cout << "Impossible d'ouvrir le fichier variable = " << name_var_2D[t] << endl << endl;
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ= swapInt(NZ);
							
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}
					
				num_subgrids = swapInt(num_subgrids);
			
				for  (int l = 0; l < num_subgrids ; l++)
				{

					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
		
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}

					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}			
				}			
			
				fclose (f);
			
				
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)
				 		{
				    			dataTemp2[lat][lon] = data[lon + lat*NX];
						
				 		}
			
				      		
		
				//definition de la variable
				NcVar *name_var;
				
				if (!(name_var = dataFile.add_var(name_var_2D[t].c_str(), ncDouble,latDim,lonDim)))
				return NC_ERR;

				// Definition des attributs de la variable
				if (!name_var->add_att("units", att_var_2D[t].c_str()))
		      		return NC_ERR;

				// Ecriture des variables donnees en argument
				if (!name_var->put(&dataTemp2[0][0], NY, NX))
		                return NC_ERR;

				cout<<"creation "<< name_var_2D[t]<<" OK" <<endl<<endl;

			}

		}	

		cout<<"fin structure fichier NC 2D " <<endl<< endl;	



		
//""""""""""""""""""""""""""" Reading mask file (BH)""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
// Read a mask file, and use it to set in the netcdf to "missing_value" cells that are outside the domain in the mask

		int MASK=0;		
		stringstream mask_filename;
		mask_filename << pth << gen << ".out.mask" << fin_fichier;  			// création du nouveau nom

	 	f = fopen(mask_filename.str().c_str(),"rb"); 					// ouverture
	 	
		if (f == NULL) 
		{
		cout << "Impossible d'ouvrir le fichier en lecture = " << mask_filename.str().c_str() << endl << endl;

		}

		else
		{
			MASK=1; //presence fichier mask

			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
	
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}		
	
			num_subgrids = swapInt(num_subgrids);
		
			for  (int i = 0; i < num_subgrids ; i++)
			{
			
				fread (&I,sizeof(int),3,f);
				fread (&n,sizeof(int),3,f);
				fread (&r,sizeof(int),3,f);
		
				for  (int j = 0; j < 3 ; j++)
				{
					n[j] = swapInt(n[j]);
					I[j] = swapInt(I[j]);
					r[j] = swapInt(r[j]);
				}
			
				for (int k = I[2]; k < I[2]+n[2] ; k++)
				{
				     for (int j = I[1]; j < I[1]+n[1] ; j++)
				     {
				     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
				     	  {
				     
				     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
				     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
				     		
					   }
				      }
				}			
			}

			fclose (f);

			for (int depth = 0; depth < NZ; depth++)
		      		for (int lat = 0; lat < NY; lat++)
			 		for (int lon = 0; lon < NX; lon++)
			 		{
						if (data[lon + lat*NX + depth*NY*NX]>0)
						data[lon + lat*NX + depth*NY*NX]=1;
						else
						data[lon + lat*NX + depth*NY*NX]=0;

			    			dataTemp[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX];
			    			
			 		}
	
		}
		
		cout<<"creation mask OK " <<endl<<endl;


//""""""""""""""""""""""""""" TRAITEMENTS DES FICHIERS PBF """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""		
		
		
		cout << "TRAITEMENTS DES FICHIERS PFB " << endl<< endl;

		cout << "Nombre de fichier pfb en lecture = "<< NTIME << endl<< endl;
	  	for (int rec = 0; rec < NTIME; rec++) 
	   	{
		 	stringstream fichier; 							  					// fichier à ouvrir
		 	fichier << pth << gen << ".out."<< dataname << "." << setfill('0') << setw(5) << rec*dumpint << fin_fichier; 	// création du nouveau nom
		 	f = fopen(fichier.str().c_str(),"rb"); 					  					// ouverture
		 	
	      		if (f == NULL) 
			cout << "Impossible d'ouvrir le fichier en lecture = " << fichier.str().c_str() << endl << endl;
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
			
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ = swapInt(NZ);				
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}		
			
				num_subgrids = swapInt(num_subgrids);
				
				for  (int i = 0; i < num_subgrids ; i++)
				{
					
					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
				
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}
					
					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}			
				}
	
			        fclose (f);
				for (int depth = 0; depth < NZ; depth++)
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)

				 		{
							if (MASK==1)				    			
							dataNC[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX]*dataTemp[depth][lat][lon];
							else
							dataNC[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX];
				    			
				 		}
							
				if (!dataVar->put_rec(&dataNC[0][0][0], rec))
		 		return NC_ERR;
	  		
  			}
      		}
      		
   	cout << "*** CONVERSION TERMINEE ***" << endl<<endl;
   
	} 
	  
    return 0;
    
    }

   }




