/*********************************************************************************************

Ce programme permet la conversion d'une série 
de fichier .pfb, générés par PARFLOW,
en un fichier au format NETCDF.
 
SYNTAX : PFBtoNC path generic_name data_name file_number DumpInterval ficher.nc units

path : 			chemin d'accès des donnée
generic_name	:	nom generique de la simulation utilisé pour nommer les fichiers PF
data_name	:	nom de la donnée
file_number	:	nombre de fichiers à lire
DumpInterval	:	Pas d'échantillonnage, utilisé pour lire les fichiers. 
			Ex une sortie en saturation à 24h: 
			genname.out.satur.00000.pfb genname.out.satur.00024.pfb...
fichier.nc	:	nom du fichier netcdf à ecrire
units		:	unités


V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Mars 2015
Modif B. HECTOR 
Février 2016
**********************************************************************************************/


#include <iostream>
#include<cstdio>
#include <cstdlib>
#include"conversionBigEndiansToLittleEndians.h"
#include <netcdfcpp.h>
#include <sstream>
#include <iomanip>

using namespace std;

static const int NC_ERR = 2; // Retour code erreur



    int main (int argc, char *argv[]) 

    {    
	if (argc !=8)
	cout << "SYNTAX : PFBtoNC path generic_name data_name file_number DumpInterval ficher.nc units" << endl;
	else
	{
	
	
//""""""""""""""""""""""""""" LECTURE PREMIER PBF """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
     		
     		string pth = argv[1];
		string gen = argv[2];
		string dataname = argv[3];

  		string fin_fichier = ".pfb"; 	// extension du fichier
		stringstream filename;
		filename << pth << gen << ".out." << dataname << ".00000" << fin_fichier;  // création du nouveau nom

     		cout<<"LECTURE DES DIMENSIONS dans fichier pfb = " << filename.str().c_str()<<endl;
		FILE * f;
		double X0[3]; 		// X,Y,Z origine
		int NX, NY, NZ;		// nombre de maille NX, NY, NZ;
		double D[3];	 	// dimension de maille DX, DY,DZ
		int I[3];		// ix, iy, iz;
		int n[3];		// nx, ny, nz;
		int r[3];		// rx, ry, rz;	
		int num_subgrids;	// nombre de sous grille
		
		
		f = fopen(filename.str().c_str(), "rb");
      		if (f == NULL)
        	cout << "Impossible d'ouvrir le fichier en lecture !" << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
			
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}
			
			num_subgrids = swapInt(num_subgrids);
					
			cout << " Point Origine : X=" << X0[0] <<" "<< "Y=" << X0[1]<< " " << "Z=" << X0[2]<< endl;
        		cout << " Nombre de X,Y,Z : NX=" << NX <<" "<< "NY=" << NY << " " << "NZ=" << NZ << endl; 
        		cout << " Dimensions : DX=" << D[0] <<" "<< "DY=" << D[1] << " " << "DZ=" << D[2] << endl;
			cout << " Nombre de mailles = "<< NX*NY*NZ << endl;
			cout << " Nombre de sous grille = "<< num_subgrids<<endl<< endl;
			fclose (f);
		}

		cout<<"PREMIERE LECTURE OK " <<endl;
		
		double data[NX*NY*NZ]; // stockage donnees lues
		cout<<"data OK " <<endl;
		
		//""""""""""""""""""""""""""""""""""""" PREPARATION DES DONNEES NETCDF """"""""""""""""""""""""""""""""""""""""""
		
		double dataNC[NZ][NY][NX];
		cout<<"dataNC OK " <<endl;

		
   		double dataTemp[NZ][NY][NX];

			
		// Creation lats, lons, depths
		int NTIME =atoi(argv[4]);
		
   		double lons[NX],lats[NY],depths[NZ],times[NTIME];
		
   				
		for (int i = 0; i < NX ; i++)
		{
			lons[i]= X0[0] + (i*D[0]) ;
		}
   		  
		for (int i = 0; i < NY ; i++)
		{
			lats[i]= X0[1] + (i*D[1]) ;
		}
   		
		for (int i = 0; i < NZ ; i++)
		{
			depths[i]= X0[2] + (i*D[2]) ;
		}
		for (int i = 0; i < NTIME ; i++)
		{
			times[i]= i;
		}

		cout<<"Creation lats, lons, depths OK " <<endl;
		


//""""""""""""""""""""""""""" Reading displacement grid (BH)"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
// Read a displacement file, used to store a new variable in the netcdf to displace cells to actual elevation position for visit rendering.

		stringstream disp_filename;
		disp_filename << pth << "dgridZ" << fin_fichier;  // création du nouveau nom
	 	f = fopen(disp_filename.str().c_str(),"rb");
		if (f == NULL) 
		cout << "Impossible d'ouvrir le fichier en lecture = " << disp_filename.str().c_str() << endl << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ= swapInt(NZ);
							
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}
					
			num_subgrids = swapInt(num_subgrids);
			
			for  (int l = 0; l < num_subgrids ; l++)
			{

				fread (&I,sizeof(int),3,f);
				fread (&n,sizeof(int),3,f);
				fread (&r,sizeof(int),3,f);
		
				for  (int j = 0; j < 3 ; j++)
				{
					n[j] = swapInt(n[j]);
					I[j] = swapInt(I[j]);
					r[j] = swapInt(r[j]);
				}

				for (int k = I[2]; k < I[2]+n[2] ; k++)
				{
				     for (int j = I[1]; j < I[1]+n[1] ; j++)
				     {
				     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
				     	  {
				     
				     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
				     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
				     		
					   }
				      }
				}			
			}			
			
			fclose (f);
			
			for (int depth = 0; depth < NZ; depth++)
		      		for (int lat = 0; lat < NY; lat++)
			 		for (int lon = 0; lon < NX; lon++)
			 		{
			    			dataTemp[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX];
						
			 		}
			
				      		
		}


		cout<<"dataTemp=displacement OK " <<endl;
		
		
	
				
//""""""""""""""""""""""""""""""""""""" CREATION FICHIER NETCDF """"""""""""""""""""""""""""""""""""""""""""""""""  			

		cout << "CREATION FICHIER NETCDF = " << argv[6]  << endl<< endl;
	
		// Creation du fichier
   		NcFile dataFile(argv[6], NcFile::Replace);

   		// Vérification de la création du fichier
   		if(!dataFile.is_valid())
      		return NC_ERR;
      		
      		// Definition des attributs globaux
      		if(!dataFile.add_att("Conventions", "CF-1.5"))
      		return NC_ERR;
		if(!dataFile.add_att("title", "Soil saturation and pressure head calculated by Parflow/CLM 3D hydrological model"))
      		return NC_ERR;
      		if(!dataFile.add_att("institution", "LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE"))
      		return NC_ERR;
      		if(!dataFile.add_att("source", "Generated by J-M. Cohard: Parflow/CLM Simulation"))
      		return NC_ERR;
      		if(!dataFile.add_att("references", "Robert, D. (2012). Caractérisation et modélisation de la dynamique de l evapotranspiration"
      		" en Afrique Soudanienne en zone de socle. Terre Univers Environnement. Grenoble, France, Université de Grenoble. PhD Thesis: 199 pp."))
      		return NC_ERR;
	
   		// Definition des dimensions
   		NcDim *timeDim, *lonDim, *latDim, *depthDim ;
      		
      		if (!(timeDim = dataFile.add_dim("time")))  // dimension = unlimited 
      		return NC_ERR;
       		if (!(lonDim = dataFile.add_dim("longitude", NX )))
      		return NC_ERR;
       		if (!(latDim = dataFile.add_dim("latitude", NY)))
      		return NC_ERR;
      		if (!(depthDim = dataFile.add_dim("depth", NZ )))
      		return NC_ERR;
   
   		// Definition des variables.
   		NcVar *lonVar, *latVar, *depthVar, *timeVar, *dXVar, *dYVar, *dZVar, *dataVar;
   
   		if (!(timeVar = dataFile.add_var("time", ncDouble, timeDim)))
   		return NC_ERR;
   		if (!(lonVar = dataFile.add_var("longitude", ncDouble, lonDim)))
      		return NC_ERR;
    		if (!(latVar = dataFile.add_var("latitude", ncDouble, latDim)))
   		return NC_ERR;
    		if (!(depthVar = dataFile.add_var("depth", ncDouble, depthDim)))
        	return NC_ERR;   
    		if (!(dXVar = dataFile.add_var("dgridX", ncDouble,depthDim,latDim,lonDim)))
        	return NC_ERR;   
    		if (!(dYVar = dataFile.add_var("dgridY", ncDouble,depthDim,latDim,lonDim)))
        	return NC_ERR;
    		if (!(dZVar = dataFile.add_var("dgridZ", ncDouble,depthDim,latDim,lonDim)))
        	return NC_ERR;
     		if (!(dataVar = dataFile.add_var(argv[3], ncDouble,timeDim,depthDim,latDim,lonDim)))
        	return NC_ERR;

   		// Definition des attributs de variables
   
   		if (!timeVar->add_att("long_name", "time"))
      		return NC_ERR;
   		if (!timeVar->add_att("standard_name", "time"))
	      	return NC_ERR;
	   	if (!timeVar->add_att("units", "seconde since 1970-01-01 00:00:00 utc"))
	      	return NC_ERR;
   
   		if (!lonVar->add_att("units", "degrees_east"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("long_name", "longitude"))
	      	return NC_ERR;
	  	 if (!lonVar->add_att("standard_name", "longitude"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("axis", "x"))
	      	return NC_ERR;
   
   		if (!latVar->add_att("units", "degrees_north"))
	      	return NC_ERR;
	   	if (!latVar->add_att("long_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("standard_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("axis", "y"))
	     	return NC_ERR;

	   	if (!depthVar->add_att("units", "meter"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("long_name", "depth from the surface"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("standard_name", "depth"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("axis", "z"))
	      	return NC_ERR;

	     	if (!dataVar->add_att("units", argv[7]))
	      	return NC_ERR;
	     	if (!dataVar->add_att("missing_value", 0))
	      	return NC_ERR;

	 	// Ecriture des variables coordonées lon lat depth
	   
	      	if (!timeVar->put(times, NTIME))
	     	return NC_ERR;
	   	if (!lonVar->put(lons, NX))
	     	return NC_ERR; 
	  	if (!latVar->put(lats, NY))
	      	return NC_ERR;
	      	if (!depthVar->put(depths, NZ))
	     	return NC_ERR;
		

/*
		for (int rec = 0; rec < NTIME; rec++)
		{
			if (!dZVar->put_rec(&dataTemp[0][0][0],rec))
	     		return NC_ERR;
		}

*/
		if (!dZVar->put(&dataTemp[0][0][0], NZ, NY, NX))
                        return NC_ERR;
		


		for (int depth = 0; depth < NZ; depth++)
		      		for (int lat = 0; lat < NY; lat++)
			 		for (int lon = 0; lon < NX; lon++)
			 		{
			    			dataTemp[depth][lat][lon] = 0;
						
			 		}

		cout<<"dataTemp=0 OK " <<endl;
/*		
		for (int rec = 0; rec < NTIME; rec++)
		{

			if (!dXVar->put_rec(&dataTemp[0][0][0],rec))
	     		return NC_ERR;    		
	      		if (!dYVar->put_rec(&dataTemp[0][0][0],rec))
	     		return NC_ERR;
		}
*/


		 if (!dXVar->put(&dataTemp[0][0][0], NZ, NY, NX))
                        return NC_ERR;
                 if (!dYVar->put(&dataTemp[0][0][0], NZ, NY, NX))
                        return NC_ERR;
		cout<<"fichier NC OK " <<endl;

		

		
		       		
	      	
		 
 		
	      	 		
		
		
		
//""""""""""""""""""""""""""" Reading mask file (BH)"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
// Read a mask file, and use it to set in the netcdf to "missing_value" cells that are outside the domain in the mask

				
		stringstream mask_filename;
		mask_filename << pth << gen << ".out.mask" << fin_fichier;  // création du nouveau nom


	 	f = fopen(mask_filename.str().c_str(),"rb"); 					  // ouverture
	 	
		if (f == NULL) 
		cout << "Impossible d'ouvrir le fichier en lecture = " << mask_filename.str().c_str() << endl << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
	
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}		
	
			num_subgrids = swapInt(num_subgrids);
		
			for  (int i = 0; i < num_subgrids ; i++)
			{
			
				fread (&I,sizeof(int),3,f);
				fread (&n,sizeof(int),3,f);
				fread (&r,sizeof(int),3,f);
		
				for  (int j = 0; j < 3 ; j++)
				{
					n[j] = swapInt(n[j]);
					I[j] = swapInt(I[j]);
					r[j] = swapInt(r[j]);
				}
			
				for (int k = I[2]; k < I[2]+n[2] ; k++)
				{
				     for (int j = I[1]; j < I[1]+n[1] ; j++)
				     {
				     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
				     	  {
				     
				     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
				     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
				     		
					   }
				      }
				}			
			}

			fclose (f);

			for (int depth = 0; depth < NZ; depth++)
		      		for (int lat = 0; lat < NY; lat++)
			 		for (int lon = 0; lon < NX; lon++)
			 		{
						if (data[lon + lat*NX + depth*NY*NX]>0)
						data[lon + lat*NX + depth*NY*NX]=1;
						else
						data[lon + lat*NX + depth*NY*NX]=0;

			    			dataTemp[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX];
			    			
			 		}
	
		}
		
		cout<<"dataTemp=mask OK " <<endl;


//""""""""""""""""""""""""""" TRAITEMENTS DES FICHIERS PBF """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""		
		
		
		cout << "TRAITEMENTS DES FICHIERS PFB " << endl;

		
		int dumpint = atoi(argv[5]);  		
  	
  		// int NTIME =atoi(argv[2]);
	  	cout << "Nombre de fichier pfb en lecture = "<< NTIME << endl<< endl;
	  	for (int rec = 0; rec < NTIME; rec++) 
	   	{
		 	stringstream fichier; 							  // fichier à ouvrir
		 	fichier << pth << gen << ".out."<< dataname << "." << setfill('0') << setw(5) << rec*dumpint << fin_fichier;  // création du nouveau nom
		 	f = fopen(fichier.str().c_str(),"rb"); 					  // ouverture
		 	
	      		if (f == NULL) 
			cout << "Impossible d'ouvrir le fichier en lecture = " << fichier.str().c_str() << endl << endl;
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
			
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ = swapInt(NZ);				
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}		
			
				num_subgrids = swapInt(num_subgrids);
				
				for  (int i = 0; i < num_subgrids ; i++)
				{
					
					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
				
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}
					
					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}			
				}
	
			        fclose (f);
				for (int depth = 0; depth < NZ; depth++)
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)
				 		{
				    			dataNC[depth][lat][lon] = data[lon + lat*NX + depth*NY*NX]*dataTemp[depth][lat][lon];
				    			
				 		}
							
				if (!dataVar->put_rec(&dataNC[0][0][0], rec))
		 		return NC_ERR;
	  		
  			}
      		}
      		
   	cout << "*** CONVERSION TERMINEE ***" << endl;
   
	} 
	  
    return 0;
    
    }





