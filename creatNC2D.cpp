/*********************************************************************************************

creatNC2D (path generic_name data_name file_number DumpInterval ficher.nc *name_var2D *att_var2D)

path : 			chemin d'accès des donnée
generic_name	:	nom generique de la simulation utilisé pour nommer les fichiers PF
numberData	:	nombre de données à traiter
numbers_dataName:	liste des numeros de donnée
file_number	:	nombre de fichiers à lire
DumpInterval	:	Pas d'échantillonnage, utilisé pour lire les fichiers. 
			Ex une sortie en saturation à 24h: 
			genname.out.satur.00000.pfb genname.out.satur.00024.pfb...
fichier		:	nom du fichier netcdf à ecrire 
			(resultat = 1 fichier par variable ex: fichier_eflx_lh_tot.nc)

name_var2D	:	liste de variables 2D à traiter
name_var2D	:	liste de unités de variables 2D à traiter

...

V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Avril 2016


**********************************************************************************************/


#include <iostream>
#include<cstdio>
#include <cstdlib>
#include"conversionBigEndiansToLittleEndians.h"
#include <netcdfcpp.h>
#include <sstream>
#include <iomanip>

#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

static const int NC_ERR = 2; // Retour code erreur


int creatNC2D ( string pth, string gen, string dataname,int numbers_dataName, int file_number, int dumpint, string out_dir, string gen_out, string *name_var2D, string *att_var2D) 

    {    
    	 
        	        	    	

	
//""""""""""""""""""""""""""" LECTURE DIMENSIONS PREMIER PBF """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
     		
     			
  		string fin_fichier = ".C.pfb"; 	// extension du fichier CLM
		stringstream filename;
		filename << pth << gen << ".out.clm_output.00001" << fin_fichier;  // création du nouveau nom
		
		FILE * f;
		double X0[3]; 		// X,Y,Z origine
		int NX, NY, NZ;		// nombre de maille NX, NY, NZ;
		double D[3];	 	// dimension de maille DX, DY,DZ
		int I[3];		// ix, iy, iz;
		int n[3];		// nx, ny, nz;
		int r[3];		// rx, ry, rz;	
		int num_subgrids;	// nombre de sous grille
		
		
		f = fopen(filename.str().c_str(), "rb");
      		if (f == NULL)
        	cout << "Impossible d'ouvrir le fichier en lecture !" << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
			
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}
			
			num_subgrids = swapInt(num_subgrids);
					
			fclose (f);
			
		}

		
		
		
		
				
		//""""""""""""""""""""""""""""""""""""" PREPARATION DES DONNEES NETCDF """"""""""""""""""""""""""""""""""""""""""
		
		double data[NX*NY*NZ]; // stockage donnees lues
		
		double dataNC2[NY][NX];
				
   		double dataTemp2[NY][NX];

			
		// Creation lats, lons, depths
		int NTIME = file_number;
		
		double lons[NX],lats[NY],depths[NZ],times[NTIME];
		
   				
		for (int i = 0; i < NX ; i++)
		{
			lons[i]= X0[0] + (i*D[0]) ;
		}
   		  
		for (int i = 0; i < NY ; i++)
		{
			lats[i]= X0[1] + (i*D[1]) ;
		}
   		
		for (int i = 0; i < NZ ; i++)
		{
			depths[i]= X0[2] + (i*D[2]) ;
		}
		for (int i = 0; i < NTIME ; i++)
		{
			times[i]= i;
		}

			
				
//""""""""""""""""""""""""""""""""""""" CREATION FICHIER NETCDF """""""""""""""""""""""""""""""""""""""""""""""""" 
	        
		stringstream filenameNC;
		filenameNC << out_dir << gen_out << "_"<< dataname<<".nc";


		cout << "Creation fichier NETCDF : " << filenameNC.str().c_str()  << endl<< endl;
	
		// Creation du fichier
   		NcFile dataFile(filenameNC.str().c_str(), NcFile::Replace);

   		
   		// Vérification de la création du fichier
   		if(!dataFile.is_valid())
      		return NC_ERR;
      		
      		
      		// Definition des attributs globaux
      		if(!dataFile.add_att("Conventions", "CF-1.5"))
      		return NC_ERR;
		if(!dataFile.add_att("title", "Soil saturation and pressure head calculated by Parflow/CLM 3D hydrological model"))
      		return NC_ERR;
      		if(!dataFile.add_att("institution", "LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE"))
      		return NC_ERR;
      		if(!dataFile.add_att("source", "Generated by J-M. Cohard: Parflow/CLM Simulation"))
      		return NC_ERR;
      		if(!dataFile.add_att("references", "Robert, D. (2012). Caractérisation et modélisation de la dynamique de l evapotranspiration"
      		" en Afrique Soudanienne en zone de socle. Terre Univers Environnement. Grenoble, France, Université de Grenoble. PhD Thesis: 199 pp."))
      		return NC_ERR;
	
   		
   		// Definition des dimensions
   		NcDim *timeDim, *lonDim, *latDim, *depthDim ;
      		
      		if (!(timeDim = dataFile.add_dim("time")))  // dimension = unlimited 
      		return NC_ERR;
       		if (!(lonDim = dataFile.add_dim("longitude", NX )))
      		return NC_ERR;
       		if (!(latDim = dataFile.add_dim("latitude", NY)))
      		return NC_ERR;
      		if (!(depthDim = dataFile.add_dim("depth", NZ )))
      		return NC_ERR;
   
   		
   		// Definition des variables.

   		NcVar *lonVar, *latVar, *depthVar, *timeVar, *dataVar;
   
   		if (!(timeVar = dataFile.add_var("time", ncDouble, timeDim)))
   		return NC_ERR;
   		if (!(lonVar = dataFile.add_var("longitude", ncDouble, lonDim)))
      		return NC_ERR;
    		if (!(latVar = dataFile.add_var("latitude", ncDouble, latDim)))
   		return NC_ERR;
    		if (!(depthVar = dataFile.add_var("depth", ncDouble, depthDim)))
        	return NC_ERR;   
    		if (!(dataVar = dataFile.add_var(dataname.c_str(), ncDouble,timeDim,latDim,lonDim)))
        	return NC_ERR;
		

   		// Definition des attributs de variables
   
   		if (!timeVar->add_att("long_name", "time"))
      		return NC_ERR;
   		if (!timeVar->add_att("standard_name", "time"))
	      	return NC_ERR;
	   	if (!timeVar->add_att("units", "seconde since 1970-01-01 00:00:00 utc"))
	      	return NC_ERR;
   
   		if (!lonVar->add_att("units", "degrees_east"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("long_name", "longitude"))
	      	return NC_ERR;
	  	 if (!lonVar->add_att("standard_name", "longitude"))
	      	return NC_ERR;
	   	if (!lonVar->add_att("axis", "x"))
	      	return NC_ERR;
   
   		if (!latVar->add_att("units", "degrees_north"))
	      	return NC_ERR;
	   	if (!latVar->add_att("long_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("standard_name", "latitude"))
	      	return NC_ERR;
	   	if (!latVar->add_att("axis", "y"))
	     	return NC_ERR;

	   	if (!depthVar->add_att("units", "meter"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("long_name", "depth from the surface"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("standard_name", "depth"))
	      	return NC_ERR;
	   	if (!depthVar->add_att("axis", "z"))
	      	return NC_ERR;

	     	if (!dataVar->add_att("units", "-"))
	      	return NC_ERR;
	     	if (!dataVar->add_att("long_name", dataname.c_str()))
	      	return NC_ERR;
	     	if (!dataVar->add_att("standard_name", dataname.c_str()))
	      	return NC_ERR;
	     	if (!dataVar->add_att("missing_value", "-99999"))
	      	return NC_ERR;

		
		// Ecriture des variables coordonées time lon lat depth
	   
	      	if (!timeVar->put(times, NTIME))
	     	return NC_ERR;
	   	if (!lonVar->put(lons, NX))
	     	return NC_ERR; 
	  	if (!latVar->put(lats, NY))
	      	return NC_ERR;
	      	if (!depthVar->put(depths, NZ))
	     	return NC_ERR;
		
		
		
		
	//boucle traitement variables 2 dimensions NX NY

	
	int numberVar2D = sizeof(name_var2D)/sizeof(int);
	cout<< "Traitement variables 2D :"<<endl;
	for (int t = 0; t < numberVar2D ; t++)

		{
			// Lecture fichier pfb variable 2D


			stringstream var_filename3;
			stringstream var_filename4;
			
			var_filename3 << pth << name_var2D[t] << ".pfb";  // création du nouveau nom
			f = fopen(var_filename3.str().c_str(),"rb");
	 		
	 		int VARFILE2D = 0;
	 		
			if (f != NULL)
				{ VARFILE2D = 1;}
			else
				{
					
					var_filename4 << pth << gen << ".out."<< name_var2D[t] << ".pfb";
					f = fopen(var_filename4.str().c_str(),"rb");
					if (f != NULL) VARFILE2D = 1;
				}				
			 
			
			if (VARFILE2D == 0 )
				cout << "Impossible d'ouvrir le fichier variable = " << name_var2D[t] << endl << endl;
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ= swapInt(NZ);
							
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}
					
				num_subgrids = swapInt(num_subgrids);
			
				for  (int l = 0; l < num_subgrids ; l++)
				{

					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
		
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}

					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}			
				}			
			
				fclose (f);
			
				
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)
				 		{
				    			dataTemp2[lat][lon] = data[lon + lat*NX];
						
				 		}
			
				      		
		
				//definition de la variable
				NcVar *name_var;
				
				if (!(name_var = dataFile.add_var(name_var2D[t].c_str(), ncDouble,latDim,lonDim)))
				return NC_ERR;

				// Definition des attributs de la variable
				if (!name_var->add_att("units", att_var2D[t].c_str()))
		      		return NC_ERR;

				// Ecriture des variables donnees en argument
				if (!name_var->put(&dataTemp2[0][0], NY, NX))
		                return NC_ERR;

				cout<<" "<< name_var2D[t]<<" OK" <<endl;

			}

		}	

		cout <<endl;

		
//""""""""""""""""""""""""""" LECTURE MASK 2D """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	


		int MASK=0;		
		stringstream mask_filename;							// création du nouveau nom
		  			
		mask_filename << pth << "top_mask.pfb";						// mask 2D
				
	 	f = fopen(mask_filename.str().c_str(),"rb"); 					// ouverture
	 	
		if (f == NULL) 
		{
		cout << "Impossible d'ouvrir le fichier en lecture = " << mask_filename.str().c_str() << endl << endl;

		}

		else
		{
			MASK=1; //presence fichier top_mask

			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
	
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}		
	
			num_subgrids = swapInt(num_subgrids);
		
			for  (int i = 0; i < num_subgrids ; i++)
			{
			
				fread (&I,sizeof(int),3,f);
				fread (&n,sizeof(int),3,f);
				fread (&r,sizeof(int),3,f);
		
				for  (int j = 0; j < 3 ; j++)
				{
					n[j] = swapInt(n[j]);
					I[j] = swapInt(I[j]);
					r[j] = swapInt(r[j]);
				}
			
				for (int k = I[2]; k < I[2]+n[2] ; k++)
				{
				     for (int j = I[1]; j < I[1]+n[1] ; j++)
				     {
				     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
				     	  {
				     
				     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
				     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
				     		
					   }
				      }
				}			
			}

			fclose (f);
			
			
			for (int lat = 0; lat < NY; lat++)
				for (int lon = 0; lon < NX; lon++)
				{
					if (data[lon + lat*NX]>0)
						data[lon + lat*NX ]=1;
					else
						data[lon + lat*NX]=0;
						
			    			dataTemp2[lat][lon] = data[lon + lat*NX];
			    			
		 		}
		 	
		 	cout<<"Mask 2D OK " <<endl<<endl;
	
		}
		
		


//""""""""""""""""""""""""""" TRAITEMENTS DES FICHIERS PBF """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""		
		
		
		//cout << "Traitement des fichiers C.PFB " << endl<< endl;

		
	  	for (int rec = 0; rec < NTIME; rec++) 
//	  	for (int rec = 6500; rec < 70081; rec++) 
	   	{
		 	stringstream fichier; 							  					// fichier à ouvrir
		 	fichier << pth << gen << ".out.clm_output."<< setfill('0') << setw(5) << (rec+1)*dumpint << fin_fichier; 	// création du nouveau nom
		 	f = fopen(fichier.str().c_str(),"rb"); 					  					// ouverture
		 	
	      		if (f == NULL) 
			cout << "Impossible d'ouvrir le fichier en lecture = " << fichier.str().c_str() << endl << endl;
			else
			{
				fread (&X0,sizeof(double),3,f);
				fread (&NX,sizeof(int),1,f);
				fread (&NY,sizeof(int),1,f);
				fread (&NZ,sizeof(int),1,f);
				fread (&D,sizeof(double),3,f);
				fread (&num_subgrids,sizeof(int),1,f);
			
				NX = swapInt(NX);
				NY = swapInt(NY);
				NZ = swapInt(NZ);
				for  (int i = 0; i < 3 ; i++)
				{
					X0[i] = swapDouble(X0[i]);
					D[i] = swapDouble(D[i]);
				}		
			
				num_subgrids = swapInt(num_subgrids);
				

				for  (int i = 0; i < num_subgrids ; i++)
				{
					
					fread (&I,sizeof(int),3,f);
					fread (&n,sizeof(int),3,f);
					fread (&r,sizeof(int),3,f);
				
					for  (int j = 0; j < 3 ; j++)
					{
						n[j] = swapInt(n[j]);
						I[j] = swapInt(I[j]);
						r[j] = swapInt(r[j]);
					}
					


					for (int k = I[2]; k < I[2]+n[2] ; k++)
					{
					     for (int j = I[1]; j < I[1]+n[1] ; j++)
					     {
					     	  for (int i = I[0]; i < I[0]+n[0] ; i++)
					     	  {
					     
					     		fread (&data[i+j*NX+k*NY*NX],sizeof(double),1,f);
					     		data[i+j*NX+k*NY*NX] = swapDouble(data[i+j*NX+k*NY*NX]);
					     		
						   }
					      }
					}
					
					
												
				}
				
				
	
			        fclose (f);
			        				
			      		for (int lat = 0; lat < NY; lat++)
				 		for (int lon = 0; lon < NX; lon++)

				 		{
							if (MASK==1)				    			
							dataNC2[lat][lon] = data[lon + lat*NX + numbers_dataName*NY*NX]*dataTemp2[lat][lon];
							else
							dataNC2[lat][lon] = data[lon + lat*NX + numbers_dataName*NY*NX];
				    			
				 		}
				 		
				 					 		
				if (!dataVar->put_rec(&dataNC2[0][0], rec))
				return NC_ERR;
	  		
  			}
      		}
      		
   	   
	} 
	
    

   




