/*********************************************************************************************
ClmPFBtoNC

Ce programme permet la conversion d'une série de fichie .C.pfb, générés par PARFLOW-C
en plusieurs fichiers ( un par data ) au format NETCDF. Cette version permet d'appliquer
un masque et d'integrer aux fichiers NETCDF des variables 2D issues de fichiers .pfb.
 
SYNTAX : ClmPFBtoNC inputCLM.txt

Le fichier inputCLM.txt contient les parametres d'entree de ClmPFPtoNC

path : 			chemin d'accès des donnée
generic_name	:	nom generique de la simulation utilisé pour nommer les fichiers PF
numberData	:	nombre de donnée a traiter
numbers_dataName:	liste des numeros de donnée
file_number	:	nombre de fichiers a lire
DumpInterval	:	Pas d'échantillonnag 
			  Ex: 24 = une sortie en saturation a 24h: 
			  genname.out.satur.00000.pfb genname.out.satur.00024.pfb...
fichier		:	nom du fichier netcdf a ecrire 
			  (resultat = 1 fichier par variable ex: fichier_eflx_lh_tot.nc)

numberVar2D	:	nombre de variables 2D a traiter puis pour chaque variable
name unit	:	nom de la variable unité de la variables 
		
	
corresondance numeros des données 2D (numbers_dataName)
exemple : 1 4 6 9

 0 = eflx_lh_tot
 1 = eflx_lwrad_out
 2 = eflx_sh_tot
 3 = eflx_soil_grnd
 4 = qflx_evap_tot
 5 = qflx_evap_grnd
 6 = qflx_evap_soi
 7 = qflx_evap_veg
 8 = qflx_tran_veg
 9 = qflx_infl
 10 = swe_out
 11 = t_grnd
 12 = pressure				 
 13 = qflx_qirr
 
 14 = tsoil ( de 14 a  NZ  => tsoil en 3D ) 			


...


V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Avril 2016


**********************************************************************************************/


#include <iostream>
#include<cstdio>
#include <cstdlib>
#include"conversionBigEndiansToLittleEndians.h"
#include"creatNC2D.h"
#include"creatNCsoil.h"
#include <netcdfcpp.h>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

static const int NC_ERR = 2; // Retour code erreur



    int main (int argc, char *argv[]) 

    {    
	if (argc != 2)
	cout << "SYNTAX : ClmPFBtoNC input.txt " << endl;
	else
	{
	
		string input_file = argv[1];  
		ifstream fichier(input_file.c_str(), ios::in);
	
		if(!fichier)  
		{
		cout << "Impossible d'ouvrir le fichier "<< input_file  << endl;
		return 0;
		}
	
		else
		{


//"""""""""""""""""""""""  LECTURE INPUT.TXT """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		cout<<endl;
		cout<<"********** LECTURE DU FICHIER DE PARAMETRES **********" << endl<<endl;
		
		string name_data[15];
		name_data[0] = "eflx_lh_tot";
		name_data[1] = "eflx_lwrad_out";
		name_data[2] = "eflx_sh_tot";
		name_data[3] = "eflx_soil_grnd";
		name_data[4] = "qflx_evap_tot";
		name_data[5] = "qflx_evap_grnd";
		name_data[6] = "qflx_evap_soi";
		name_data[7] = "qflx_evap_veg";
		name_data[8] = "qflx_tran_veg";
		name_data[9] = "qflx_infl";
		name_data[10] = "swe_out";
		name_data[11] = "t_grnd";
		name_data[12] = "h_top";				 
		name_data[13] = "qflx_qirr";
		name_data[14] = "tsoil";
		
		
		
		string pth, gen, gen_out, root_dir_out, fichierNC;
		stringstream out_dir;
		int NTIME, dumpint, numberData, numberVar2D;
		char curr_date [50];

		fichier >> pth >> gen >> numberData;        	
        	
        	int numbers_dataName[numberData];
        	
        	cout << "Data a traiter :"<< endl;
        	
        	for (int s = 0; s < numberData ; s++)
 		{
 			fichier >> numbers_dataName[s];
 			if ( numbers_dataName[s] < 0 | numbers_dataName[s] > 14 )
 			{
 				cout << "Numero de data non valide ( doit etre compris entre 0 et 14)"<<endl;
 				return 0;
			}
			else
 			cout <<" "<< name_data[numbers_dataName[s]]<<endl;
 			
 		}
        	
        	
        	fichier >> NTIME >> dumpint >> root_dir_out >> gen_out ;

    		time_t t = time(0);   // get time now
		struct tm * now = localtime( & t );		
		sprintf(curr_date,"%4d_%02d_%02d",(now->tm_year + 1900),(now->tm_mon + 1),now->tm_mday);
                out_dir <<root_dir_out<<gen_out<<"_" << curr_date << "/";  // création du nouveau dossier
		mkdir(out_dir.str().c_str(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

       		 		
 		fichier >> numberVar2D;
		cout<<endl;
		cout<<"Variables 2D a traiter :"<< endl;
		
		string name_var2D[numberVar2D];
 		string att_var2D[numberVar2D];
		
		for (int t = 0; t < numberVar2D ; t++)
 		{
 			fichier >> name_var2D[t] >> att_var2D[t];
 			cout<<" "<< name_var2D[t] <<" "<< att_var2D[t]<<endl;
 		}
                
                cout<<endl;
		cout << "Nombre de fichier c.pfb = "<< NTIME << endl<< endl;
		
            	fichier.close();	
		
		
		
		//""""""""""""""""""""""""""" LECTURE PREMIER PBF """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""	
     		
     			
  		string fin_fichier = ".C.pfb"; 	// extension du fichier CLM
		stringstream filename;
		filename << pth << gen << ".out.clm_output.00001" << fin_fichier;  // création du nouveau nom
		
		cout<<"********** LECTURE DES DIMENSIONS **********"<< endl<<endl;
		cout<<"fichier pfb = " << filename.str().c_str()<<endl<<endl;
		
		
		FILE * f;
		double X0[3]; 		// X,Y,Z origine
		int NX, NY, NZ;		// nombre de maille NX, NY, NZ;
		double D[3];	 	// dimension de maille DX, DY,DZ
		int I[3];		// ix, iy, iz;
		int n[3];		// nx, ny, nz;
		int r[3];		// rx, ry, rz;	
		int num_subgrids;	// nombre de sous grille
		
		
		f = fopen(filename.str().c_str(), "rb");
      		if (f == NULL)
        	cout << "Impossible d'ouvrir le fichier en lecture !" << endl;
		else
		{
			fread (&X0,sizeof(double),3,f);
			fread (&NX,sizeof(int),1,f);
			fread (&NY,sizeof(int),1,f);
			fread (&NZ,sizeof(int),1,f);
			fread (&D,sizeof(double),3,f);
			fread (&num_subgrids,sizeof(int),1,f);
			
			NX = swapInt(NX);
			NY = swapInt(NY);
			NZ = swapInt(NZ);
			
			for  (int i = 0; i < 3 ; i++)
			{
				X0[i] = swapDouble(X0[i]);
				D[i] = swapDouble(D[i]);
			}
			
			num_subgrids = swapInt(num_subgrids);
					
			cout << " Point Origine : X=" << X0[0] <<" "<< "Y=" << X0[1]<< " " << "Z=" << X0[2]<< endl;
        		cout << " Nombre de X,Y,Z : NX=" << NX <<" "<< "NY=" << NY << " " << "NZ=" << NZ << endl; 
        		cout << " Dimensions : DX=" << D[0] <<" "<< "DY=" << D[1] << " " << "DZ=" << D[2] << endl;
			cout << " Nombre de mailles = "<< NX*NY*NZ << endl;
			cout << " Nombre de sous grille = "<< num_subgrids<<endl<< endl;
			fclose (f);
			
		}

		
		
// debut boucle pour chaque data en Z		
		
        	for (int s = 0; s < numberData ; s++)
        	{
 			cout<<endl;
 			cout<< "********** TRAITEMENT DATA : "<< name_data[numbers_dataName[s]]<< " **********"<<endl<<endl;
 		
 			if (numbers_dataName[s]!=14)
 			{
 			 	creatNC2D(pth, gen, name_data[numbers_dataName[s]],numbers_dataName[s], NTIME, dumpint, out_dir.str(), gen_out, name_var2D, att_var2D);
 			
 			}
 			else
 			{	cout<<"soil"<<endl;
 				creatNCsoil(pth, gen, name_data[numbers_dataName[s]],numbers_dataName[s], NTIME, dumpint, out_dir.str(), gen_out, name_var2D, att_var2D);
 			}	
 			cout<< "DATA : "<< name_data[numbers_dataName[s]]<< " OK "<<endl<<endl;	
 		}
        	        	    	
// fin boucle pour chaque data en Z		
		
	
	
		cout << endl;
		cout <<"******************************"<< endl<<endl;
   		cout << "    CONVERSION TERMINEE " << endl<<endl;
   		cout <<"******************************"<< endl<<endl;
   
		} 
	  
    		return 0;
    
    }

   }




