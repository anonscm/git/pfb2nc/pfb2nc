#!/bin/bash

source /applis/ciment/v2/env.bash
module load ciment/devel_gcc-4.6.2 
module load netcdf/4.1.3_gcc-4.6.2
#name0="Vshape_exp_20m"
#name1="clay_normal"
#name2="clay_normal"
#name3="2016_05_13"

#name0="gravi_cube_hillslope"
#name1="results2_saka_z1_1cm_w9_low_thr"
#name2="gravi"
#name3="2016_05_13"

name0="Oueme_sup"
name1="oueme_5yrs"
name2="oueme_5yrs"
name3="2016_05_17"


# Vshape

#./PFBtoNC input_vshape_press.txt
#./PFBtoNC input_vshape_sat.txt
#./ClmPFBtoNC inputCLM_vshape.txt

#cp /scratch/basileh/simus/$name0/$name1/forcagePF.txt.0 /home/basileh/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /scratch/basileh/simus/$name0/$name1/vshape.tcl /home/basileh/outputs/$name2"_"$name3/simu.tcl
#cp /scratch/basileh/simus/$name0/$name1/*domains.pfb /home/basileh/outputs/$name2"_"$name3/$name2"_"domains.pfb

# Cube gravi

#./PFBtoNC input_gravi_press.txt
#./PFBtoNC input_gravi_sat.txt
#./ClmPFBtoNC inputCLM_gravi.txt

#cp /scratch/basileh/simus/$name0/$name1/forcagePF.txt.0 /home/basileh/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /scratch/basileh/simus/$name0/$name1/cube.tcl /home/basileh/outputs/$name2"_"$name3/simu.tcl
#cp /scratch/basileh/simus/$name0/$name1/cube.pfb /home/basileh/outputs/$name2"_"$name3/$name2"_"domains.pfb

# COueme

#./PFBtoNC input_oueme_press.txt
#./PFBtoNC input_oueme_sat.txt
#./ClmPFBtoNC inputCLM_oueme.txt

#cp /scratch/basileh/simus/$name0/$name1/forcagePF.txt.0 /home/basileh/outputs/$name2"_"$name3/$name2""forcagePF.txt
#cp /scratch/basileh/simus/$name0/$name1/oueme.tcl /home/basileh/outputs/$name2"_"$name3/simu.tcl
#cp /scratch/basileh/simus/$name0/$name1/oueme_domains.pfb /home/basileh/outputs/$name2"_"$name3/$name2"_"domains.pfb
