/*********************************************************************************************

CONVERSION BIG ENDIANS to LITTLE ENDIANS

Les fichiers pfb generes par PARFLOW sont des fichiers binaires en BIG ENDIANS

V.QUATELA
LTHE (Laboratoire d etude des Transferts en Hydrologie et en Environnement), Grenoble, FRANCE
Mars 2015

**********************************************************************************************/


double swapDouble(double);
int swapInt(int );
